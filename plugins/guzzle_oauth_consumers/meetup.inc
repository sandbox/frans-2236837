<?php

$plugin = array(
  'fullname' => 'MeetUp',
  'form alter' => 'meetup_form_alter',
  'load consumer class' => 'guzzle_oauth_meetup_load_consumer_class',
);

/**
 * We want to use the same names as used on meetup.com
 * Meetup does not use scopes.
 */
function meetup_form_alter(&$form, &$form_state) {
  $form['config']['consumer_key']['#title'] = t('Key');
  $form['config']['consumer_secret']['#title'] = t('Secret');
  $form['config']['scope']['#access'] = FALSE;
}